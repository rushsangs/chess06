package chess;
/**
* Enum for the alphabetical columns
* @author Ronish Desai, Rushit Sanghrajka
*
*/
public enum Column {
	a(0), 
	b(1),
	c(2),
	d(3),
	e(4),
	f(5),
	g(6),
	h(7);

	private int value;
	
	/**
	 * Sets the value of the column
	 * @param value integer used to set the column
	 */
	private Column(int value) {
		this.value = value;
	}
	/**
	 * Gets the value of the column given the char 
	 * @param x char representation of the column
	 * @return integer value of the char column
	 */
	public static int getValue(char x){
		int ret = -1;
		for(Column c: Column.values()){
			if(c.name().charAt(0) == x){
				ret = c.getValue();
			}
		}
		return ret;
	}
	
	/**
	 * Gets the value of the column
	 * @return intger representation of the column
	 */
	public int getValue() {
		return value;
	}

}