package chess;

import java.util.ArrayList;
import java.util.List;

import pieces.*;

public class ChessBoard {

	public String[][] board = new String[8][8];
	static List<ChessPiece> attackers = new ArrayList<ChessPiece>();
	static List<Integer> attacker_rows = new ArrayList<Integer>();
	static List<Integer> attacker_cols = new ArrayList<Integer>();
	static boolean trackAttackers = false;

	/**
	 * 
	 * The ChessBoard class holds the board itself, and performs all the
	 * necessary tasks related to the game board.
	 * 
	 * @author Ronish Desai, Rushit Sanghrajka
	 * 
	 * 
	 *         NOTE TO OURSELVES: While moving a chess piece from A to B, change
	 *         string in chess board for position A to empty string and call
	 *         colorBoard
	 */
	public void initialize() {

		colorBoard();
//
//		board[7][Column.a.getValue()] = "bR";
//		board[7][Column.b.getValue()] = "bN";
//		board[7][Column.c.getValue()] = "bB";
//		board[7][Column.d.getValue()] = "bQ";
//		board[7][Column.e.getValue()] = "bK";
//		board[7][Column.f.getValue()] = "bB";
//		board[7][Column.g.getValue()] = "bN";
//		board[7][Column.h.getValue()] = "bR";
//		board[6][Column.a.getValue()] = "bp";
//		board[6][Column.b.getValue()] = "bp";
//		board[6][Column.c.getValue()] = "bp";
//		board[6][Column.d.getValue()] = "bp";
//		board[6][Column.e.getValue()] = "bp";
//		board[6][Column.f.getValue()] = "bp";
//		board[6][Column.g.getValue()] = "bp";
//		board[6][Column.h.getValue()] = "bp";
//
//		board[0][Column.a.getValue()] = "wR";
//		board[0][Column.b.getValue()] = "wN";
//		board[0][Column.c.getValue()] = "wB";
//		board[0][Column.d.getValue()] = "wQ";
//		board[0][Column.e.getValue()] = "wK";
//		board[0][Column.f.getValue()] = "wB";
//		board[0][Column.g.getValue()] = "wN";
//		board[0][Column.h.getValue()] = "wR";
//		board[1][Column.a.getValue()] = "wp";
//		board[1][Column.b.getValue()] = "wp";
//		board[1][Column.c.getValue()] = "wp";
//		board[1][Column.d.getValue()] = "wp";
//		board[1][Column.e.getValue()] = "wp";
//		board[1][Column.f.getValue()] = "wp";
//		board[1][Column.g.getValue()] = "wp";
//		board[1][Column.h.getValue()] = "bp";
//		board[0][Column.a.getValue()] = "wK";
		
		//STALEMATE CONDITION:
//		board[4][Column.g.getValue()] = "wK";
//		board[6][Column.g.getValue()] = "wB";
//		board[7][Column.g.getValue()] = "bK";
		
//		board[1][Column.a.getValue()] = "bR";
		board[0][Column.b.getValue()] = "wQ";
		board[0][Column.f.getValue()] = "wK";
		board[4][Column.g.getValue()] = "wR";
		board[6][Column.b.getValue()] = "wR";
//		board[7][Column.d.getValue()] = "bQ";
		board[7][Column.h.getValue()] = "bK";
		
		
	}

	/**
	 * Gets the color code of the piece given a location
	 * 
	 * @param r
	 *            row that contains the piece
	 * @param c
	 *            column that contains the piece
	 * @return 'w' for white and 'b' for black and 'n' for neither
	 */
	public char getColorCode(int r, int c) {
		char code = 'n';
		if (board[r][c].contains("w")) {
			code = 'w';
			return code;
		} else if (board[r][c].contains("b")) {
			code = 'b';
			return code;
		} else {
			return code;
		}
	}

	/**
	 * Gets the Piece at the specified location
	 * 
	 * @param r
	 *            row that contains the piece
	 * @param c
	 *            column that contains the piece
	 * @return ChessPiece at the given location
	 */
	public ChessPiece getPiece(int r, int c) {
		ChessPiece piece = null;
		if (board[r][c].contains("R")) {
			piece = new Rook();
			piece.name = "Rook";
			return piece;
		} else if (board[r][c].contains("N")) {
			piece = new Knight();
			piece.name = "Knight";
			return piece;
		} else if (board[r][c].contains("B")) {
			piece = new Bishop();
			piece.name = "Bishop";
			return piece;
		} else if (board[r][c].contains("Q")) {
			piece = new Queen();
			piece.name = "Queen";
			return piece;
		} else if (board[r][c].contains("K")) {
			if (board[r][c].contains("w"))
				piece = new King('w');
			else
				piece = new King('b');
			piece.name = "King";
			return piece;
		} else if (board[r][c].contains("p")) {
			if (board[r][c].contains("wp")) {
				piece = new Pawn('w');
			} else if (board[r][c].contains("bp")) {
				piece = new Pawn('b');
			}
			piece.name = "Pawn";
			return piece;
		} else if (board[r][c].contains("  ") && board[r][c].contains("##")) {
			return piece;
		} else {
			return piece;
		}
	}

	/**
	 * Adds the color to the entire board (either '##' or ' ')
	 */
	protected void colorBoard() {
		for (int i = 0; i < board.length; ++i) {
			for (int j = 0; j < board[0].length; ++j) {
				if (board[i][j] == "" || board[i][j] == null) {
					if ((i + j) % 2 == 0)
						board[i][j] = "##";
					else
						board[i][j] = "  ";
				}
			}
		}
	}

	/**
	 * Adds the color to the board given a set of locations (either '##' or ' ')
	 * This method is implicitly called by colorBoard(), use the no-args method
	 * in most cases.
	 * 
	 * @param row
	 *            row number for the block to be colored
	 * @param column
	 *            column number for the block to be colored.
	 */
	public void colorBoard(int row, int column) {
		if (board[row][column] == "") {
			if ((row + column) % 2 == 0)
				board[row][column] = "##";
			else
				board[row][column] = "  ";
		}
	}

	/**
	 * Draws the board using ASCII characters
	 */
	public void drawBoard() {
		for (int i = 7; i >= 0; --i) {
			for (int j = Column.a.getValue(); j <= Column.h.getValue(); ++j) {
				System.out.print(board[i][j] + " ");
			}
			System.out.print(i + 1);
			System.out.println();
		}

		for (char i = 'a'; (int) i < (int) 'i'; ++i) {
			System.out.print((char) i + "  ");
		}
		System.out.println();
	}

	/**
	 * Checks if opponents_piece passed to it attacks the king. This method is
	 * implicitly called by the other isCheck and only checks for Check
	 * condition from one piece. In most cases, use the other isCheck method
	 * which doesn't need the opponent piece's details.
	 * 
	 * @param playercolor
	 *            Colorcode of the opponent team
	 * @param kings_location_row
	 *            Row index for the calling player's king
	 * @param kings_location_col
	 *            Column index for the calling player's king
	 * @param row
	 *            Row index for the opponent's piece
	 * @param col
	 *            Column index for the opponent's piece
	 * @param opponents_piece
	 *            The Chesspiece object for the opponent's chesspiece.
	 * @return true if the opponent's chesspiece specified does check the
	 *         players' king.
	 */
	public boolean isCheck(char playercolor, int kings_location_row, int kings_location_col, int row, int col,
			ChessPiece opponents_piece) {
		int l = attackers.size();
		if (opponents_piece == null)
			return false;
		if (opponents_piece.isValid(row, col, kings_location_row, kings_location_col)
				&& opponents_piece.isPossible(board, row, col, kings_location_row, kings_location_col)) {
			if (trackAttackers) {
				attackers.add(opponents_piece);
				attacker_cols.add(col);
				attacker_rows.add(row);
			}
			else
				return true;
			
		}
		if (l == attackers.size())
			return false;
		return true;
	}

	/**
	 * Loops through ALL opponent's pieces to check if they attack king.
	 * 
	 * @param playercolor
	 *            color of opponent
	 * @param kings_location_row
	 *            row index for player's king
	 * @param kings_location_col
	 *            column index for player's king
	 * @return true if player's king is under check by any of the opponent's
	 *         chess pieces.
	 */
	public boolean isCheck(char playercolor, int kings_location_row, int kings_location_col) {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if ((board[i][j].indexOf(playercolor) >= 0)
						&& isCheck(playercolor, kings_location_row, kings_location_col, i, j, getPiece(i, j)))
					return true;
			}
		}
		return false;
	}

	/**
	 * Returns true if stale mate has occurred
	 * @param playercolor color of the opponent/ possibly winning player
	 * @param kings_location_row king's row index
	 * @param kings_location_col king's column index
	 * @param recent_move_row Row index for most recent move
	 * @param recent_move_column Column index for most recent move
	 * @return true if stalemate has occurred
	 */
	public boolean isStaleMate(char playercolor, int kings_location_row, int kings_location_col, int recent_move_row,
			int recent_move_column) {
		King k;
		if (playercolor == 'b')
			k = new King('w');
		else
			k = new King('b');
		attackers = new ArrayList<ChessPiece>();
		trackAttackers=true;
		// move king in every possible direction and see what happens
		if ((k.isValid(kings_location_row, kings_location_col, kings_location_row + 1, kings_location_col) && k
				.isPossible(board, kings_location_row, kings_location_col, kings_location_row + 1, kings_location_col))
				|| (k.isValid(kings_location_row, kings_location_col, kings_location_row - 1, kings_location_col)
						&& k.isPossible(board, kings_location_row, kings_location_col, kings_location_row - 1,
								kings_location_col))
				|| (k.isValid(kings_location_row, kings_location_col, kings_location_row, kings_location_col + 1)
						&& k.isPossible(board, kings_location_row, kings_location_col, kings_location_row,
								kings_location_col + 1))
				|| (k.isValid(kings_location_row, kings_location_col, kings_location_row, kings_location_col - 1)
						&& k.isPossible(board, kings_location_row, kings_location_col, kings_location_row,
								kings_location_col - 1))
				|| (k.isValid(kings_location_row, kings_location_col, kings_location_row + 1, kings_location_col + 1)
						&& k.isPossible(board, kings_location_row, kings_location_col, kings_location_row + 1,
								kings_location_col + 1))
				|| (k.isValid(kings_location_row, kings_location_col, kings_location_row + 1, kings_location_col - 1)
						&& k.isPossible(board, kings_location_row, kings_location_col, kings_location_row + 1,
								kings_location_col - 1))
				|| (k.isValid(kings_location_row, kings_location_col, kings_location_row - 1, kings_location_col + 1)
						&& k.isPossible(board, kings_location_row, kings_location_col, kings_location_row - 1,
								kings_location_col + 1))
				|| (k.isValid(kings_location_row, kings_location_col, kings_location_row - 1, kings_location_col - 1)
						&& k.isPossible(board, kings_location_row, kings_location_col, kings_location_row - 1,
								kings_location_col - 1)) || (((trackAttackers = false) == true))) {
			trackAttackers = false;
			return false;
			//see if any of the attackers can be attacked.
			
		} else if (isCheck(playercolor, recent_move_row, recent_move_column)) { // other
																				// pieces
																				// can
																				// attack
																				// the
																				// checkmate
																				// piece
																				// TODO
			return false;
		}
		for(int l = 0; l<attackers.size();++l){
			if(playercolor=='w'){
			
			if(isCheck('b', attacker_rows.get(l), attacker_cols.get(l))){
				return false;
			}
		}else
		{
			if(isCheck('w', attacker_rows.get(l), attacker_cols.get(l))){
				return false;
			}
		}
				
		}
		return true;
	}

	/**
	 * Returns true if checkmate has occured
	 * 
	 * @param playercolor
	 *            color of the opponent/ possibly winning player
	 * @param kings_location_row
	 *            recent/current player's king's row index
	 * @param kings_location_col
	 *            recent/current player's king's column index
	 * @param recent_move_row
	 *            recent/ current player's recent move row index
	 * @param recent_move_column
	 *            recent/ current player's recent move column index
	 * @return true if the recent player checkmates the opponent
	 */
	public boolean isCheckMate(char playercolor, int kings_location_row, int kings_location_col, int recent_move_row,
			int recent_move_column) {
		if (isStaleMate(playercolor, kings_location_row, kings_location_col, recent_move_row, recent_move_column)
				&& isCheck(playercolor, kings_location_row, kings_location_col)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Gets the current king's position given the color
	 * 
	 * @param color
	 *            either is black or white
	 * @return an integer array that contains the location of the king
	 */
	public int[] getKingPosition(char color) {
		for (int i = 0; i < 8; ++i) {
			for (int j = 0; j < 8; ++j) {
				if (getPiece(i, j) instanceof King && board[i][j].indexOf(color) >= 0)
					return new int[] { i, j };
			}
		}
		return null;
	}
}