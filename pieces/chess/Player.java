package chess;

/**
 * The Player class stores Player specific features and attributes
 * 
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
public class Player {
	/**
	 * Stores the player's color: 'w' or 'b'
	 */
	char colorCode;

	/**
	 * Returns the color code of the player
	 * 
	 * @return color code 'w' or 'b'
	 */
	public char getColorCode() {
		return colorCode;
	}

	private boolean isActive;

	/**
	 * Configures the Player
	 * 
	 * @param color
	 *            Character, 'b' for black player and 'w' for white player.
	 */
	public Player(char color) {
		this.colorCode = color;
		if (color == 'b')
			isActive = false;
		else
			isActive = true;
	}

	/**
	 * Changes the Player's turn
	 */
	protected void changeTurn() {
		isActive = !isActive;
	}

	/**
	 * Gets players active status
	 * 
	 * @return player's status
	 */
	public boolean getActive() {
		return isActive;
	}

	/**
	 * Moves the piece to the proper location
	 * 
	 * @param cb
	 *            chess board object
	 * @param sr
	 *            Initial row number for the chess piece.
	 * @param sc
	 *            Initial column for the chess piece.
	 * @param er
	 *            Row number that the player wants to move the chess piece to.
	 * @param ec
	 *            Column that the player wants to move the chess piece to.
	 */
	protected void movePiece(ChessBoard cb, int sr, int sc, int er, int ec) {
		cb.board[er][ec] = cb.board[sr][sc];
		cb.board[sr][sc] = "";
	}

	/**
	 * Undo's the previous move
	 * 
	 * @param cb
	 *            chessboard object
	 */
	protected void undoMove(ChessBoard cb) {
		String previous = Log.previousTurn();
		String[] temp = Log.previousTurn().split(":");
		String location = temp[1];
		String[] piece = temp[0].split("@");

		System.out.println(location);

		int sr = Character.getNumericValue(location.charAt(1));
		int sc = Character.getNumericValue(location.charAt(0));
		int er = Character.getNumericValue(location.charAt(4));
		int ec = Character.getNumericValue(location.charAt(3));

		System.out.println("SR:" + sr + " EC:" + sc + " ER:" + er + " EC:" + ec);

		cb.board[sr][sc] = cb.board[er][ec];
		cb.board[er][ec] = piece[1];
		Log.log.remove(previous);

		/*
		 * System.out.println("===================="); for(String s: Log.log){
		 * System.out.println(s); } System.out.println("====================");
		 */
	}
}