package chess;

import java.util.Scanner;
import pieces.*;

/**
 * This is the main class which controls and synchronizes the chess board, players and chess pieces.
 * @author Ronish, Rush
 * We have also done the extra credit
 */
public class Chess {
	private boolean checkDetected;
	private boolean mateDetected;
	private boolean staleDetected;
	private boolean drawDetected;
	private boolean kingAttacked;
	private char promoteTo;
	private ChessBoard cb;
	private Player white;
	private Player black;
	protected Scanner scn;
	
	/**
	 *  This method creates the two players
	 */
	void createPlayers() {
		white = new Player('w');
		black = new Player('b');
	}
	
	/**
	 *  This method initilaizes chessboard and sets all the chess pieces
	 */
	void startGame(){
		cb = new ChessBoard();
		cb.initialize();
		promoteTo = 'Q';
		checkDetected = false;
		drawDetected = false;
		mateDetected = false;
		staleDetected = false;
		kingAttacked = false;
	}
	
	/**
	 * This method handles the commands that are passed in by the player
	 * @param chess Passes the object chess
	 * @param player Passes the object Player
	 * @param in Integer array with the set of locations
	 * @return true if the command was executed, false otherwise
	 */
	boolean handleCommand(Chess chess, Player player, int[] in){
		int sr = in[0];
		int sc = in[1];
		int er = in[2];
		int ec = in[3];
		
		ChessPiece p = null;
		if((p = chess.cb.getPiece(sr, sc)) != null){
			if(player.getColorCode() != cb.getColorCode(sr, sc)){					//meaning white is moving a black peice or black is moving a white piece
				return false;
			} else if((p.isValid(sr, sc, er, ec) && (p.isPossible(chess.cb.board, sr, sc, er, ec)))){  //else if the move is a valid and possible move
				String endPiece = chess.cb.board[er][ec];
				player.movePiece(chess.cb, sr, sc, er, ec);							//then move the piece
				
				if(p instanceof Pawn && ((Pawn)p).readyForPromotion(er)){			//if piece is a pawn and ready for promotion
					((Pawn)p).promote(chess.cb, player, promoteTo, er, ec);			//promote the pawn
					Log.writeToLog(chess.cb.board[er][ec]+"@"+endPiece+":"+sc+sr+" "+ec+er+" "+promoteTo);	//write to log
				} 
				Log.writeToLog(chess.cb.board[er][ec]+"@"+endPiece+":"+sc+sr+" "+ec+er);	//write to log
				chess.cb.colorBoard(sr, sc);												//color the board
				
				if(checkDetected){															//checks to see if the current player was undercheck
					int[] kings_position = cb.getKingPosition(player.getColorCode()); 		//gets the current players king's position
					if(player.getColorCode() == 'w' && chess.cb.isCheck('b', kings_position[0], kings_position[1])) { 	//if current player is white check to see if black pieces attack the white king
						player.undoMove(chess.cb);
						return false;
					} else if(player.getColorCode() == 'b' && chess.cb.isCheck('w', kings_position[0], kings_position[1])) {	//if current player is black check to see if white pieces attack the black king
						player.undoMove(chess.cb);
						return false;
					} else {
						checkDetected = false;
					}
				}
				
				//TODO If king is taken out, getKingPostition goes to NULL (FIX THIS!!!)
				if(player.colorCode=='w'){
					int[] kings_position = cb.getKingPosition('b');
					if((kings_position = cb.getKingPosition('b')) == null){
						kingAttacked = true;
					} else if(chess.cb.isCheckMate('w', kings_position[0], kings_position[1], er, ec)) {
						mateDetected = true;
					} else if(chess.cb.isStaleMate('w', kings_position[0], kings_position[1], er, ec)) {
						staleDetected = true;
					} else if(chess.cb.isCheck('w', kings_position[0], kings_position[1])) {
						checkDetected = true;
					}
				} else {
					int[] kings_position = cb.getKingPosition('w');
					if((kings_position = cb.getKingPosition('w')) == null){
						kingAttacked = true;
					} else if(chess.cb.isCheckMate('b', kings_position[0], kings_position[1], er, ec)) {
						mateDetected = true;
					} else if(chess.cb.isStaleMate('b', kings_position[0], kings_position[1], er, ec)) {
						staleDetected = true;
					} else if(chess.cb.isCheck('b', kings_position[0], kings_position[1])) {
						checkDetected = true;
					}
				}
				promoteTo = 'Q';
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * This method reads the user's command and executes the right decisions based on the user's command
	 * @param player Type of the player
	 * @return The set of location passed by the player in an integer array
	 */
	int[] readInput(String player){
		scn = new Scanner(System.in);
		if(checkDetected){
			System.out.println("\ncheck");
		} else if(mateDetected || kingAttacked){
			if(player.compareTo("white") == 0){
				System.out.println("\nBlack Wins!");
			} else {
				System.out.println("\nWhite Wins!");
			}
			System.exit(0);
		} else if(staleDetected){
			System.out.println("\nStalemate! Game is a draw!");
			System.exit(0);
		}
		String input = "";
		System.out.println("\nEnter set of moves (Player "+ player + "):");
		if((input = scn.nextLine()) != null){
			String[] tokens = input.split(" ");
			if(tokens.length == 1){
				if(tokens[0].equalsIgnoreCase("resign")){
					if(player.contains("white")){
						System.out.println("Black wins!");
					} else {
						System.out.println("White wins!");
					}
					System.exit(0);
				} else if(tokens[0].equalsIgnoreCase("draw") && drawDetected){
					System.out.println("Draw!");
					System.exit(0);
				} /*else if(tokens[0].equalsIgnoreCase("undo")){
					if(player.contains("white")){
						white.undoMove(cb);
					} else {
						black.undoMove(cb);
					}
				} */else {
					return null;
				}
			}
			else {					//two commands
				if((tokens.length == 2 || tokens.length == 3) && tokens[0].length() == 2 && tokens[1].length() == 2){
					if(drawDetected == true){			//if the user doesn't reply with draw(only occuse whrn they dont)
						drawDetected = false;
					} 
					
					int sc = Column.getValue(tokens[0].charAt(0));
					int sr = Character.getNumericValue(tokens[0].charAt(1)) - 1;
					int ec = Column.getValue(tokens[1].charAt(0));	
					int er = Character.getNumericValue(tokens[1].charAt(1)) - 1;
				
					//System.out.println("SR:"+ sr + " SC:" + sc +" ER:"+ er + " EC:" + ec);
					System.out.println("");
					int[] in = new int[4];
					in[0] = sr;
					in[1] = sc;
					in[2] = er;
					in[3] = ec;
				
					if((sr < 0 || sr > 7) || (sc < 0 || sc > 7) || (er < 0 || er > 7) || (ec < 0 || ec > 7)){		//checks to see if its a valid input
						drawDetected = false;
						return null;													
					} else {
						if((tokens.length == 3) && (tokens[2].length() == 1) 
								&& ((cb.getPiece(sr, sc)) instanceof Pawn) && (er == 7 || er == 0)){
							char p = tokens[2].charAt(0);
							if(p == 'N' || p == 'B' || p == 'Q' || p == 'R'){
								promoteTo = p;
								return in;
							} else {
								return null;
							}
						} else if((tokens.length == 3) && (tokens[2].toLowerCase().contains("draw?"))){		//if the user asks for draw
							drawDetected = true;
							return in;
						} else if(tokens.length == 3) {
							return null;
						} else {
							return in;
						}
					}
				} else {
					return null;
				}
			}
		}
		return null;
	}
	
	/**
	 * This is the main method, which has the implementation for the what should be done in each turn.
	 * @param args main method arguments.
	 */
	public static void main(String[] args){
		Chess chess = new Chess();
		int pi[];
		chess.createPlayers();
		chess.startGame();
		while(true){
			if(chess.white.getActive() == true){
				chess.cb.drawBoard();									//draws the board
				if((pi = chess.readInput("white")) != null){			//reads the input and returns an int array
					if(chess.handleCommand(chess, chess.white, pi) == true) {	//handles the player input
						chess.white.changeTurn();								//changes the turn
						chess.black.changeTurn();
					} else {
						System.out.println("Invalid Move \n");
					}	
				} else {
					System.out.println("Illegal Input \n");
				}
			} else if(chess.black.getActive() == true){
				chess.cb.drawBoard();									//draws the board
				if((pi = chess.readInput("black")) != null){				//reads the input and returns an int array
					if(chess.handleCommand(chess, chess.black, pi) == true) {	//handles the player input
						chess.black.changeTurn();								//changes the turn
						chess.white.changeTurn();
					} else {
						System.out.println("Invalid Move \n");
					}
				} else {
					System.out.println("Illegal Input \n");
				}
			}
		}
		//scn.close();	
	}
}