package pieces;

import chess.*;
/**
* Pawn chesspiece, part of the chess game.
* @author Ronish Desai, Rushit Sanghrajka
*
*/
public class Pawn extends ChessPiece{
	private char type;
	private int[] enpassant; 
	
	public Pawn(char c){													//'w' for white and 'b' for black
		type = c;

	}
	
	public boolean isPossible(String[][] chessboard, int startrow, int startcolumn, int endrow, int endcolumn){
 		if(isValidLocation(startrow, startcolumn)==false || isValidLocation(endrow, endcolumn) ==false) {
			return false;
		}
		
		if(isSameLocation(startrow, startcolumn, endrow, endcolumn) == true){
			return false;
		}
		
		if(isSameType(chessboard, startrow, startcolumn, endrow, endcolumn) == true){
			return false;
		}
		
		int initialrow = -1;												//stores the location of the pawn's of either side (1 for white and 6 for black)
		if(chessboard[startrow][startcolumn].contains("b")){				//checks to see if the starting piece is black 
			initialrow = 6;
		} else if(chessboard[startrow][startcolumn].contains("w")){			//checks to see if the starting piece is white 
			initialrow = 1;
		}
		
		if(startcolumn == endcolumn){										//if the pawn is moved in the same column
			if((startrow != initialrow) && (Math.abs((endrow - startrow)) == 2)){	//if the pawn is not at its initial location and moved 2 spaces
				return false;
			} else {
				if(endrow > startrow){
					for(int i = startrow+1; i <= endrow; i++){							//search from start point till end point for strings other than '##' or '  '
						if(!((chessboard[i][startcolumn].contains("##")) || (chessboard[i][startcolumn].contains("  ")))){
							//System.out.println("The path contains a piece");
							return false;
						}
					}
				} else {
					for(int i = startrow-1; i >= endrow; i--){							//search from start point till end point for strings other than '##' or '  '
						if(!((chessboard[i][startcolumn].contains("##")) || (chessboard[i][startcolumn].contains("  ")))){
							//System.out.println("The path contains a piece");
							return false;
						}
					}
				}
			}
			return true;
		} else if(startcolumn != endcolumn){								//if the pawn is moved diagonally (meaning its taking out another piece)
			if((chessboard[endrow][endcolumn] == "##")||(chessboard[endrow][endcolumn] == "  ")){	//check to see if there is another piece in the end point
				if(isEnpassant()){
					int locationrow = enpassant[1];
					int locationcolumn = enpassant[0];
					if((locationrow == startrow) && ((locationcolumn+1 == startcolumn) || (locationcolumn-1 == startcolumn))){
						chessboard[locationrow][locationcolumn] = "  ";
						return true;
					} else {
						return false;
					}
				} else {
					//System.out.println("Cant move diagonally if the spot doesnt have a piece");
					return false;
				}
			} else {
				//System.out.println("The move is diagonal and the end spot has a piece");
				return true;
			}
		} else {
			return false;
		}
	}
	
	public boolean isValid(int startrow, int startcolumn, int endrow, int endcolumn){
		if((startrow < 0 || startrow > 7) || (startcolumn < 0 || startcolumn > 7) || (endrow < 0 || endrow > 7) || (endcolumn < 0 || endcolumn > 7)){
			//System.out.println("coordinates are valid");
			return false;													//checks to see if the indexes passed are valid
		}
		
		if(((startrow+1) == endrow) && (startcolumn == endcolumn) && (type == 'w')) {	//checks to see if the white pawn moved forward by 1
			return true;
		} else if(((startrow-1) == endrow) && (startcolumn == endcolumn) && (type == 'b')) {	//checks to see if the black pawn moved forward by 1													//else it only moved forward by 1
			return true;
		} else if((((startcolumn+1) == endcolumn) || ((startcolumn-1) == endcolumn)) //diagonally left or right
				&& ((((startrow+1) == endrow) && type =='w') || (((startrow-1) == endrow) && type == 'b'))){		//checks to see if the white pawn moved diagonally left or right
			return true;
		} else if((startrow == 1) && ((startrow+2) == endrow) && (type == 'w')){	//checks to see if the white pawn moved forward by 2
			return true;
		} else if((startrow == 6) && ((startrow-2) == endrow) && (type == 'b')){	//checks to see if the black pawn moved forward by 2
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks whether the pawn move was an enpassant move.
	 * @return True if the move was an enpassant.
	 */
	public boolean isEnpassant(){
		String[] temp = Log.previousTurn().split(":");
		String[] previous = temp[0].split("@");
		String location = temp[1];
		int sr = Character.getNumericValue(location.charAt(1));
		int er = Character.getNumericValue(location.charAt(4));
		int ec = Character.getNumericValue(location.charAt(3));
		
		//System.out.println("SR:"+sr+" ER:"+er + " EC: "+ ec);
		if(((previous[0].contains("wp")) && (sr == 1) && (er == 3)) || ((previous[0].contains("bp")) && (sr == 6) && (er == 4))){
			//System.out.println("ENPASSANT CASE");
			enpassant = new int[2];
			enpassant[0] = ec;
			enpassant[1] = er;
			//System.out.println("EC: " + enpassant[0] + " ER:"+er);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks whether the pawn is ready to be promoted
	 * @param row row index of the position of the pawn object.
	 * @return true if pawn is ready for promotion.
	 */
	
	public boolean readyForPromotion(int row){
		if((type == 'w' && row == 7) || (type == 'b' && row == 0)){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Promotes the Pawn to another chesspiece depending on the player's choice.
	 * @param cb the chessboard object
	 * @param player the player who owns the pawn
	 * @param s the new piece to be promoted to
	 * @param row row index of the pawn
	 * @param col column index of the pawn
	 */

	public void promote(ChessBoard cb, Player player, char s, int row, int col){
		String piece = Character.toString(player.getColorCode())+Character.toString(s);
		cb.board[row][col] = piece;
	}
}