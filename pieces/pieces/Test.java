package pieces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import chess.ChessBoard;
import chess.Column;
/**
 * A simple tester class implemented to test if a move is possible or not. This is not part of the project requirements and is an internal implementation.
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
public class Test {
	private static BufferedReader input;

	public static void main(String[] args) throws IOException{			//going to use this to make sure the chess pieces function properly
		ChessBoard cb = new ChessBoard();
		cb.initialize();
		cb.drawBoard();
		input = new BufferedReader(new InputStreamReader(System.in));	
		
		while(true){
			System.out.print("\n" + "Enter set of locations to move: ");
			String message;
			while((message = input.readLine()) != null){
				char s = message.charAt(0);
				char e = message.charAt(3);
				int sc = Column.getValue(s);
				int sr = Character.getNumericValue(message.charAt(1)) - 1;
				int ec = Column.getValue(e);	
				int er = Character.getNumericValue(message.charAt(4)) - 1;
				
				System.out.println("SR:"+ sr + " SC:" + sc +" ER:"+ er + " EC:" + ec);
				
				String type = cb.getPiece(sr, sc).name;
				System.out.println("Type: " + type);
				if(type.compareTo("Rook") == 0){
					Rook rook = new Rook();
					System.out.println("Valid: "+ rook.isValid(sr, sc, er, ec) + " Possible: " + rook.isPossible(cb.board, sr, sc, er, ec));
				} else if(type.compareTo("Bishop") == 0){
					Bishop bishop = new Bishop();
					System.out.println("Valid: "+ bishop.isValid(sr, sc, er, ec) + " Possible: " + bishop.isPossible(cb.board, sr, sc, er, ec));
				} else if(type.compareTo("King") == 0){
					King king;
					if(cb.board[sr][sc].contains("w"))
						king = new King('w');
					else
						king = new King('b');
					System.out.println("Valid: "+ king.isValid(sr, sc, er, ec) + " Possible: " + king.isPossible(cb.board, sr, sc, er, ec));
				} else if(type.compareTo("Queen") == 0){
					Queen queen = new Queen();
					System.out.println("Valid: "+ queen.isValid(sr, sc, er, ec) + " Possible: " + queen.isPossible(cb.board, sr, sc, er, ec));
				} else if(type.compareTo("Knight") == 0){
					Knight knight = new Knight();
					System.out.println("Valid: "+ knight.isValid(sr, sc, er, ec) + " Possible: " + knight.isPossible(cb.board, sr, sc, er, ec));
				} else if(type.compareTo("Pawn") == 0){
					Pawn pawn = null;
					if(cb.board[sr][sc].contains("wp")){
						pawn = new Pawn('w');
						System.out.println("white");
					} else if(cb.board[sr][sc].contains("bp")){
						pawn = new Pawn('b');
						System.out.println("black");
					}
					System.out.println("Valid: "+ pawn.isValid(sr, sc, er, ec) + " Possible: " + pawn.isPossible(cb.board, sr, sc, er, ec));
				}
				System.out.print("Enter set of locations to move: ");
			}
		
		}
	}
}