package pieces;

/**
 * Chess piece declares the methods that must be implemented by other solid
 * chess pieces in order to work in a Chess game.
 * 
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
public abstract class ChessPiece {
	public String name;

	/**
	 * Returns true if the chess piece can be moved from start position to new
	 * position. It internally calls isValid. This method must also check
	 * whether there are any other pieces in the path of the active piece.
	 * 
	 * @param chessboard
	 *            Configuration of pieces on the chess board.
	 * @param startrow
	 *            Initial row number for the chess piece.
	 * @param startcolumn
	 *            Initial column for the chess piece.
	 * @param endrow
	 *            Row number that the player wants to move the chess piece to.
	 * @param endcolumn
	 *            Column that the player wants to move the chess piece to.
	 * @return True if move is possible.
	 */
	public abstract boolean isPossible(String[][] chessboard, int startrow, int startcolumn, int endrow, int endcolumn);

	/**
	 * This is an internal method which checks if a move is possible.
	 * Chess-piece specific moves must be implemented here.
	 * 
	 * @param startrow
	 *            Initial row number for the chess piece.
	 * @param startcolumn
	 *            Initial column for the chess piece.
	 * @param endrow
	 *            Row number that the player wants to move the chess piece to.
	 * @param endcolumn
	 *            Column that the player wants to move the chess piece to.
	 * @return True if move is a valid move for the particular chess piece.
	 */
	public abstract boolean isValid(int startrow, int startcolumn, int endrow, int endcolumn);

	/**
	 * Checks if a row-column value pair is valid
	 * 
	 * @param row
	 *            Row on chess board
	 * @param column
	 *            Column on chess board
	 * @return true if row-column pair is valid
	 */
	protected boolean isValidLocation(int row, int column) {
		if ((row < 0 || row > 7) || (column < 0 || column > 7) || (row < 0 || row > 7) || (column < 0 || column > 7)) {
			return false;
		}
		return true;
	}

	/**
	 * Checks if the piece that is moving is not moving to a location with the
	 * same type of a piece
	 * 
	 * @param chessboard
	 *            Configuration of pieces on the chess board.
	 * @param startrow
	 *            Initial row number for the chess piece.
	 * @param startcolumn
	 *            Initial column for the chess piece.
	 * @param endrow
	 *            Row number that the player wants to move the chess piece to.
	 * @param endcolumn
	 *            Column that the player wants to move the chess piece to.
	 * @return true if valid and false if it's not
	 */
	protected boolean isSameType(String[][] chessboard, int startrow, int startcolumn, int endrow, int endcolumn) {
		if (chessboard[startrow][startcolumn].contains("b")) { // checks to see
																// if the
																// starting
																// piece is
																// black then
																// the piece at
																// the endpoint
																// cannot be
																// black
			if (chessboard[endrow][endcolumn].contains("b")) {
				return true;
			}
		} else if (chessboard[startrow][startcolumn].contains("w")) { // checks
																		// to
																		// see
																		// if
																		// the
																		// starting
																		// piece
																		// is
																		// white
																		// then
																		// the
																		// piece
																		// at
																		// the
																		// endpoint
																		// cannot
																		// be
																		// white
			if (chessboard[endrow][endcolumn].contains("w")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the piece that is moving to the same location as it is located
	 * previously
	 * 
	 * @param startrow
	 *            Initial row number for the chess piece.
	 * @param startcolumn
	 *            Initial column for the chess piece.
	 * @param endrow
	 *            Row number that the player wants to move the chess piece to.
	 * @param endcolumn
	 *            Column that the player wants to move the chess piece to.
	 * @return true if valid and false if it's not
	 */
	protected boolean isSameLocation(int startrow, int startcolumn, int endrow, int endcolumn) {
		if ((startrow == endrow) && (startcolumn == endcolumn)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Prints out the name of the piece
	 */
	public String toString() {
		return name;
	}
}