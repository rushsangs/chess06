package pieces;

import java.util.ListIterator;

import chess.ChessBoard;
import chess.Log;
/**
 * The King chesspiece, part of the chess game.
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
public class King extends ChessPiece {
	char color;

	public King(char color) {
		this.color = color;
	}

	public boolean isPossible(String[][] chessboard, int startrow, int startcolumn, int endrow, int endcolumn) {
		if (isValidLocation(startrow, startcolumn) == false || isValidLocation(endrow, endcolumn) == false) {
			return false;
		}

		if (isSameLocation(startrow, startcolumn, endrow, endcolumn) == true) {
			return false;
		}

		if (isSameType(chessboard, startrow, startcolumn, endrow, endcolumn) == true) {
			return false;
		}
		
		if((startrow == 7 && startcolumn == 4 && startrow == endrow && Math.abs(endcolumn - startcolumn) == 2) 						//checks if king is attempting castiling
				|| (startrow == 0 && startcolumn == 4 && startrow == endrow && Math.abs(endcolumn - startcolumn) == 2)){
			int initialrow = -1;
			if(color == 'w'){
				initialrow = 0;
			} else if (color == 'b'){
				initialrow = 7;
			}
			
			ChessBoard cb = new ChessBoard();
			cb.board = chessboard;
			if(endcolumn < startcolumn){
				for(int i = 0; i <= 4; i ++){
					if(!(chessboard[initialrow][i].contains("##")) && !(chessboard[initialrow])[i].contains("  ")){
						if(i == 0 || i == 4 || i == 7){													
							if(ifMoved(initialrow, i)){					//checks if either the rooks or king has moved previously
								return false;
							}
						} else {
							return false;
						}
					}
				}
				
				if(color == 'w'){
					while(startcolumn >= endcolumn){						//loops through the each position that the king passes
						if(cb.isCheck('b', startrow, startcolumn)){					//checks to see if either of them are in check
							return false;
						}
						startcolumn--;
						
					}
					chessboard[endrow][endcolumn+1] = "wR";				//moves the rook to the right
					chessboard[initialrow][0] = "  ";					
				} else if (color == 'b'){
					while(startcolumn >= endcolumn){						//loops through the each position that the king passes
						if(cb.isCheck('w', startrow, startcolumn)){		//checks to see if either of them are in check
							return false;
						}
						startcolumn--;
					}
					chessboard[endrow][endcolumn+1] = "bR";				//moves the rook to the right
					chessboard[initialrow][0] = "  ";
				}
				
			} else {
				for(int i = 4; i <= 7; i ++){
					if(!(chessboard[initialrow][i].contains("##")) && !(chessboard[initialrow])[i].contains("  ")){
						if(i == 0 || i == 4 || i == 7){													
							if(ifMoved(initialrow, i)){					//checks to see if either rooks or king has moved previously
								return false;
							}
						} else {
							return false;
						}
					}
				}
				
				if(color == 'w'){
					while(startcolumn <= endcolumn){	  				//loops through the each position that the king passes
						if(cb.isCheck('b', startrow, startcolumn)){		//checks to see if either of them are in check
							return false;
						}
						startcolumn++;
					}
					chessboard[endrow][endcolumn-1] = "wR";				//moves the rook to the left
					chessboard[initialrow][7] = "  ";
				} else if (color == 'b'){
					while(startcolumn <= endcolumn){	 				//loops through the each position that the king passes
						if(cb.isCheck('w', startrow, startcolumn)){					//checks to see if either of them are in check
							return false;
						}
						startcolumn++;
					}
					chessboard[endrow][endcolumn-1] = "bR";				//moves the rook to the left
					chessboard[initialrow][7] = "  ";
				}
			}
			cb.colorBoard();
		}

		ChessBoard c = new ChessBoard();
		c.board = chessboard;
		String oldendpiece ="";
		String oldstartpiece ="";
		// isPossible should perform the move and then check if Check condition
		// occurs
		if (c.board[startrow][startcolumn].indexOf("K") > 0) {
			oldendpiece = c.board[endrow][endcolumn];
			oldstartpiece = c.board[startrow][startcolumn];
			
			c.board[endrow][endcolumn] = c.board[startrow][startcolumn];
			c.board[startrow][startcolumn] = "  ";
			c.colorBoard(startrow, startcolumn);
		}
		if (this.color == 'w' && !c.isCheck('b', endrow, endcolumn)) {
			c.board[startrow][startcolumn] = oldstartpiece;
			c.board[endrow][endcolumn] = oldendpiece;
			
			return true;
		} else if(this.color == 'b' && !c.isCheck('w', endrow, endcolumn)) {
			c.board[startrow][startcolumn] = oldstartpiece;
			c.board[endrow][endcolumn] = oldendpiece;
			return true;
		}
		c.board[startrow][startcolumn] = oldstartpiece;
		c.board[endrow][endcolumn] = oldendpiece;
		return false;
		
	}

	public boolean isValid(int startrow, int startcolumn, int endrow, int endcolumn) {
		if ((startrow < 0 || startrow > 7) || (startcolumn < 0 || startcolumn > 7) || (endrow < 0 || endrow > 7)
				|| (endcolumn < 0 || endcolumn > 7)) {
			return false; // checks to see if the indexes passed are valid
		}
		
		if((startrow == 7 && startcolumn == 4 && startrow == endrow && Math.abs(endcolumn - startcolumn) == 2) 
				|| (startrow == 0 && startcolumn == 4 && startrow == endrow && Math.abs(endcolumn - startcolumn) == 2)){ 	//if king moved 2 spaces left/right as the first move 
			return true;
		}

		if (((startrow == endrow) || (startcolumn == endcolumn))
				|| (Math.abs(startrow - endrow) == Math.abs(startcolumn - endcolumn))) { // checks to see if the king moved forward or left/right
			if (((startrow + 1) == endrow) || ((startrow - 1) == endrow)) { // checks to see if the king moved 1 space forward/backward
				return true;
			} else if (((startcolumn + 1) == endcolumn) || ((startcolumn - 1) == endcolumn)) { // checks to see if the king moved 1 space left/right
				return true;
			} else if ((Math.abs(startrow - endrow) == Math.abs(startcolumn - endcolumn))
					&& (Math.abs(startrow - endrow) == 1)) { // checks to see if the king moved 1 diagonally
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Checks to see if the piece has moved or not (used for castling)
	 * @param row the row of the piece in question
	 * @param column the column of the piece in question
	 * @return true if moved or else false
	 */
	public boolean ifMoved(int row, int column){
		 ListIterator<String> litr = null;
		 litr = Log.log.listIterator();
		 
		 while(litr.hasNext()){
			 String[] temp = litr.next().split(":");
			 //System.out.println(temp[0]+temp[1]);
			 String location = temp[1];
			 int sr = Character.getNumericValue(location.charAt(1));
			 int sc = Character.getNumericValue(location.charAt(0));
			 
			 if(sr == row && sc == column){
				 return true;
			 }
		 }
		 return false;
	}
}