package pieces;
/**
 * Rook chesspiece for the chess game.
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
public class Rook extends ChessPiece {

	public boolean isPossible(String[][] chessboard, int startrow, int startcolumn, int endrow, int endcolumn){
		if(isValidLocation(startrow, startcolumn)==false || isValidLocation(endrow, endcolumn) ==false){
			return false;
		}
		
		if(isSameLocation(startrow, startcolumn, endrow, endcolumn) == true){
			return false;
		}
		
		if(isSameType(chessboard, startrow, startcolumn, endrow, endcolumn) == true){
			return false;
		}
		
		if(startcolumn == endcolumn){											//check to see if rook moved vertically
			if(startrow < endrow) {												//if endrow is greater
				for(int i = startrow+1; i < endrow; i++){						//search from start point till end point for strings other than '##' or '  '
					if(!(chessboard[i][startcolumn].contains("##")) && !(chessboard[i][startcolumn].contains("  "))){
						return false;
					}
				}
			} else {															//if endrow is less
				for(int i = startrow-1; i < endrow; i--){						//search from start point till end point for strings other than '##' or '  '
					if(!(chessboard[i][startcolumn].contains("##")) && !(chessboard[i][startcolumn].contains("  "))){
						return false;
					}
				}
			}
			return true;
		} else if(startrow == endrow){											//check to see if rook moved horizontally
			if(startcolumn < endcolumn){										//if endcolumn is greater
				for(int i = startcolumn+1; i < endcolumn; i++){					//search from start point till end point for strings other than '##' or '  '
					if(!(chessboard[startrow][i].contains("##")) && !(chessboard[startrow][i].contains("  "))){
						return false;
					}
				}
			} else {															//else endcolumn is less
				for(int i = startcolumn-1; i < endcolumn; i--){					//search from start point till end point for strings other than '##' or '  '
					if(!(chessboard[startrow][i].contains("##")) && !(chessboard[startrow][i].contains("  "))){
						return false;
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isValid(int startrow, int startcolumn, int endrow, int endcolumn){
		if((startrow < 0 || startrow > 7) || (startcolumn < 0 || startcolumn > 7) || (endrow < 0 || endrow > 7) || (endcolumn < 0 || endcolumn > 7)){
			return false;													//checks to see if the indexes passed are valid
		}
			
		if((startrow == endrow) || (startcolumn == endcolumn)) {			//checks to see if the rook moved forward or left/right 
			return true;
		} else {	
			return false;
		}
	}
}