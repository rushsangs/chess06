package pieces;
/**
 * The Bishop chess piece, part of the Chess game.
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
public class Bishop extends ChessPiece{
	
	public boolean isPossible(String[][] chessboard, int startrow, int startcolumn, int endrow, int endcolumn){
		if(isValidLocation(startrow, startcolumn)==false || isValidLocation(endrow, endcolumn) ==false){
			return false;
		}
		
		if(isSameLocation(startrow, startcolumn, endrow, endcolumn) == true){
			return false;
		}
		
		if(isSameType(chessboard, startrow, startcolumn, endrow, endcolumn) == true){
			return false;
		}
		
		if(startrow < endrow && startcolumn < endcolumn){						//moving diagonally forwards
			startrow++;
			startcolumn++;
			while((startrow < endrow) && (startcolumn < endcolumn)){	 		//search from start point till end point for strings other than '##' or '  '
				if(!(chessboard[startrow][startcolumn].contains("##")) && !(chessboard[startrow][startcolumn].contains("  "))){
					return false;
				}
				startrow++;															
				startcolumn++;
			}
		} else if(startrow < endrow && startcolumn > endcolumn){						//moving diagonally forwards
			startrow++;
			startcolumn--;
			while((startrow < endrow) && (startcolumn > endcolumn)){	 		//search from start point till end point for strings other than '##' or '  '
				if(!(chessboard[startrow][startcolumn].contains("##")) && !(chessboard[startrow][startcolumn].contains("  "))){
					return false;
				}
				startrow++;															
				startcolumn--;
			}
		} else if(startrow > endrow && startcolumn < endcolumn){						//moving diagonally forwards
			startrow--;
			startcolumn++;
			while((startrow < endrow) && (startcolumn < endcolumn)){	 		//search from start point till end point for strings other than '##' or '  '
				if(!(chessboard[startrow][startcolumn].contains("##")) && !(chessboard[startrow][startcolumn].contains("  "))){
					return false;
				}
				startrow--;															
				startcolumn++;
			}
		} else {																//moving diagonally backwards
			startrow--;
			startcolumn--;
			while((startrow > endrow) && (startcolumn > endcolumn)){	 		//search from start point till end point for strings other than '##' or '  '
				if(!(chessboard[startrow][startcolumn].contains("##")) && !(chessboard[startrow][startcolumn].contains("  "))){
					return false;
				}
				startrow--;															
				startcolumn--;
			}
		}
		return true;
	}
	
	public boolean isValid(int startrow, int startcolumn, int endrow, int endcolumn){
		if((startrow < 0 || startrow > 7) || (startcolumn < 0 || startcolumn > 7) || (endrow < 0 || endrow > 7) || (endcolumn < 0 || endcolumn > 7)){
			return false;													//checks to see if the indexes passed are valid
		}
			
		if(Math.abs(startrow - endrow) == Math.abs(startcolumn - endcolumn)) {				//checks to see if the bishop moved diagonally
			return true;
		} else {
			return false;
		}
	}
}