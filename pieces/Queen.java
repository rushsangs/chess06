package pieces;
/**
 * Queen chessspiece for the chess game
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
public class Queen extends ChessPiece{

	public boolean isPossible(String[][] chessboard, int startrow, int startcolumn, int endrow, int endcolumn){
		if(isValidLocation(startrow, startcolumn)==false || isValidLocation(endrow, endcolumn) ==false){
			return false;
		}
		
		if(isSameLocation(startrow, startcolumn, endrow, endcolumn) == true){
			return false;
		}
		
		if(isSameType(chessboard, startrow, startcolumn, endrow, endcolumn) == true){
			return false;
		}
		
		if(startcolumn == endcolumn){										//checks to see if queen moved vertically
			if(startrow < endrow){											//if endrow is greater
				for(int i = startrow+1; i < endrow; i++){					//search from start point till end point for strings other than '##' or '  '
					if(!(chessboard[i][startcolumn].contains("##")) && !(chessboard[i][startcolumn].contains("  "))){
						return false;
					}
				}
			} else {														//else endrow is smaller
				for(int i = startrow-1; i > endrow; i--){					//search from start point till end point for strings other than '##' or '  '
					if(!(chessboard[i][startcolumn].contains("##")) && !(chessboard[i][startcolumn].contains("  "))){
						return false;
					}
				}
			}
			return true;
		} else if(startrow == endrow){										//checks to see if queen moved horizontally
			if(startcolumn < endcolumn){									//if endcolumn is grearter
				for(int i = startcolumn+1; i < endcolumn; i++){				//search from start point till end point for strings other than '##' or '  '
					if(!(chessboard[startrow][i].contains("##")) && !(chessboard[startrow][i].contains("  "))){
						return false;
					}
				}
			} else {															//else endcolumn is less
				for(int i = startcolumn-1; i > endcolumn; i--){					//search from start point till end point for strings other than '##' or '  '
					if(!(chessboard[startrow][i].contains("##")) && !(chessboard[startrow][i].contains("  "))){
						return false;
					}
				}
			}
			return true;
		} else if(Math.abs(startrow - endrow) == Math.abs(startcolumn - endcolumn)) {		//checks to see if queen moved diagonally
			Bishop queen = new Bishop();
			return queen.isPossible(chessboard, startrow, startcolumn, endrow, endcolumn);
		}
		return false;
	}
	
	public boolean isValid(int startrow, int startcolumn, int endrow, int endcolumn){
		if((startrow < 0 || startrow > 7) || (startcolumn < 0 || startcolumn > 7) || (endrow < 0 || endrow > 7) || (endcolumn < 0 || endcolumn > 7)){
			return false;													//checks to see if the indexes passed are valid
		}
			
		if(((startrow == endrow) || (startcolumn == endcolumn)) || (Math.abs(startrow - endrow) == Math.abs(startcolumn - endcolumn))) {	//checks to see if the rook moved forward or left/right 
			return true;
		} else {	
			return false;
		}
	}
}