package pieces;
/**
 * Knight chesspiece, part of the Chess game.
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
public class Knight extends ChessPiece {

	public boolean isPossible(String[][] chessboard, int startrow, int startcolumn, int endrow, int endcolumn){
		if(isValidLocation(startrow, startcolumn)==false || isValidLocation(endrow, endcolumn) ==false){
			return false;
		}
		
		if(isSameLocation(startrow, startcolumn, endrow, endcolumn) == true){
			return false;
		}
		
		if(isSameType(chessboard, startrow, startcolumn, endrow, endcolumn) == true){
			return false;
		}
		return true;
	}
	
	public boolean isValid(int startrow, int startcolumn, int endrow, int endcolumn){
		if((startrow < 0 || startrow > 7) || (startcolumn < 0 || startcolumn > 7) || (endrow < 0 || endrow > 7) || (endcolumn < 0 || endcolumn > 7)){
			return false;													//checks to see if the indexes passed are valid
		}
			
		if(((startrow+2) == endrow) || ((startrow-2) == endrow)) {			//checks to see if the knight moved forward or backwards by 2
			if(((startcolumn+1) == endcolumn) || ((startcolumn-1) == endcolumn)){	//checks to see if the knight moved 1 left or right
				return true;
			} else {																//else it only moved forward or backwards by 2
				return false;
			}
		} else if(((startcolumn+2) == endcolumn) || ((startcolumn-2) == endcolumn)){	//checks to see if the knight moved right or left by 2
			if(((startrow+1) == endrow) || ((startrow-1) == endrow)){		//checks to see if the knight moved 1 forward or backward
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}