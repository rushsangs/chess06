package chess;

/**
 * Log keeps track of all the moves in the game, suitable for future reference and implementation of undo.
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
import java.util.ArrayList;
import java.util.List;

public class Log {
	public static List<String> log = new ArrayList<String>();
	
	/**
	 * Writes the given string to log (Format -> <MovingPieceName>|<PieceAtEndLocation>:<startcolumn><startrow> <endcolumn><endrow>)
	 * @param s string that is need to be logged
	 */
	public static void writeToLog(String s){
		log.add(s);
	}
	
	/**
	 * Gets the input of the previous turn
	 * @return String format of the previous input
	 */
	public static String previousTurn(){
		return log.get(log.size()-1);
	}
	
}